#!/bin/bash
#
# Copyright 2012 Google Inc. All Rights Reserved.
# Author: asharif@google.com (Ahmad Sharif)

tar -cjf profile.tbz2 --owner=root --group=root "$@"
