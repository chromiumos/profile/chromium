#!/bin/bash
#
# Copyright 2012 Google Inc. All Rights Reserved.
# Author: asharif@google.com (Ahmad Sharif)

git remote add somelabel ssh://gerrit.chromium.org:29418/chromiumos/profile/chromium.git
git push somelabel HEAD:refs/for/master  # Creates Gerrit Issue


